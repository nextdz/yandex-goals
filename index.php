<?php
/*
  Plugin Name: Яндекс цели
  Description: Добавление целей яндекс метрики на отправку писем через CF7, 02.07.2018 
  Version: 1.2
  Author: Yanush Andrey
  Author URI: http://web-lab.men
  Copyright: Yanush Andrey

 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class yam {

    private $OPTION_NAME,
            $NONCE,
            $fieldRows,
            $CN,
            $RName,
            $yamROWS,
            $yamIdVal,
            $yamArray;

    public function __construct() {


        $this->OPTION_NAME = get_class($this) . '_settings';
        $this->NONCE = get_class($this) . "_settings_nonce";
        $this->RName = get_class($this) . '_row';

        $this->CN = get_class($this);
        $this->yamID = $this->CN . 'ID';

//        add_action('wp_footer', array(&$this, 'randerScript'), 9999); /*Вывод больше не нужен, все работает теперь через файл js, eval() - решила проблему */
        add_action('wp_enqueue_scripts', array(&$this, 'enqueue_scripts'));
        add_action('init', array(&$this, 'enqueue_adnin_scripts'));
        add_action('admin_menu', array(&$this, 'addSettingsPage'));
    }

    public function addSettingsPage() {

        add_options_page('Яндекс цели',
                'ЯНДЕКС ЦЕЛИ',
                'delete_pages',
                'wingi_' . get_class($this) . '_settings',
                array(&$this, 'settingsPage')
        );
    }

    public function enqueue_adnin_scripts() {
        if (is_admin()):
            wp_register_script('yam-js', plugins_url('assets/js/yam.js', __FILE__));
            wp_enqueue_script('yam-js');

            wp_register_style('yam-css', plugins_url('assets/css/yam.css', __FILE__));
            wp_enqueue_style('yam-css');

        endif;
    }

    public function enqueue_scripts() {


        wp_register_script('frontYam', plugins_url('assets/js/front-yam.js', __FILE__),array('jquery'), null, true);
        wp_enqueue_script('frontYam');


        $opt = $this->getOptoons();
        wp_localize_script('frontYam', 'yamObj', $opt);
    }

    public function getOptoons() {
        $option = get_option($this->OPTION_NAME);

        if (!empty($option)) {
            $option = unserialize($option);
            $option['yaCounter'] = 'yaCounter'.$option['yamID'];
            return $option;
        } else {
            return FALSE;
        }
    }

    public function settingsPage() {

        $lastRow = 'last_row';
        $countRow = 1;

        if (isset($_POST[$this->RName])) {
            $opt[$this->yamID] = $_POST[$this->yamID];
            $opt[$this->RName] = $_POST[$this->RName];


            update_option($this->OPTION_NAME, serialize($opt));
        }
        $option = $this->getOptoons();
        if ($option !== FALSE) {

            $this->yamIdVal = $option[$this->yamID];
            $this->yamROWS = $option[$this->RName];

            $temp = '';

            foreach ($this->yamROWS as $kay => $val) {
                if (count($this->yamROWS) != $countRow) {
                    $lastRow = '';
                }
                $this->fieldRows .= "<div class='tergetRow rowCount-{$countRow} {$lastRow}'  data-row='{$this->RName}' data-count='{$countRow}'>
                        <input class='formID' type='text' placeholder='id формы' value='{$val['formID']}' name='{$this->RName}[{$countRow}][formID]' />
                        <input class='targetID' type='text' placeholder='id цели' value='{$val['targetID']}' name='{$this->RName}[{$countRow}][targetID]' />
                        <input class='descrTerget' type='text' placeholder='Название цели' value='{$val['descrTerget']}' name='{$this->RName}[{$countRow}][descrTerget]' />
                    </div>";
                $countRow++;
            }
        } else {

            $this->fieldRows = "<div class='tergetRow rowCount-{$countRow} {$lastRow}' data-row='{$this->RName}' data-count='{$countRow}'>
                        <input class='formID' type='text' placeholder='id формы' value='' name='{$this->RName}[{$countRow}][formID]' />
                        <input class='targetID' type='text' placeholder='id цели' value='' name='{$this->RName}[{$countRow}][targetID]' />
                        <input class='descrTerget' type='text' placeholder='Название цели' value='' name='{$this->RName}[{$countRow}][descrTerget]' />
                    </div>";
        }
        ?>

        <div id="yam" class="yam-wrapp">
            <h1>Яндекс цели</h1>
            <form method="post" action="">

                <div class="yam-id">
                    <label>yaCounter</label>
                    <input type="text" value="<?= $this->yamIdVal; ?>" name="<?= $this->yamID; ?>" />
                <hr></div> 
                <?= $this->fieldRows; ?>
                <input type="hidden" name="<?= $this->NONCE ?>" value="<?= wp_create_nonce(__FILE__); ?>" />
                <?php submit_button('Сохранить цели'); ?>
            </form>
        </div>

        <?php
    }

    public function randerScript() {
        $option = $this->getOptoons();
        $res = "<!--noindex-->
                <script>
                window.onload = function(){
		function yandexGoal(goal){try{yaCounter{$option[$this->yamID]}.reachGoal(goal); /*console.log(goal)*/}catch(err){console.error('Ошибка цели' + goal +' - '+ err);}}
                }		
                </script>
                <!--/noindex-->";
        echo $res;
    }

}

$YAM = new yam();
