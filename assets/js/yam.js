jQuery(document).ready(function ($) {
    var add_button = '<span class="add_button">&#10010;</span>';

    function addBtn() {
        $('#yam .tergetRow').last().addClass('last_row');
        $('#yam').find('.last_row').append('<span class="add_row">Добавить<span>');
    }

    function deleteBtn() {
        $('#yam .tergetRow').append('<span class="delete_row">Удалить<span>');
    }

    deleteBtn();
    addBtn();


    $('#yam').on('click', '.add_row', function () {
        var $lastRow = $('#yam').find('.last_row');
        var dataCount = $($lastRow).data('count');
        var dataRow = $($lastRow).data('row');
        var cCount = dataCount + 1;

        $($lastRow).find('.add_row').remove();
        $($lastRow).removeClass('last_row');

        var row = "<div class='tergetRow rowCount-" + cCount + " ' data-row='" + dataRow + "' data-count='" + cCount + "'>\n\
                            <input class='formID' type='text' placeholder='id формы' value='' name='" + dataRow + "[" + cCount + "][formID]' />\n\
                            <input class='targetID' type='text' placeholder='id цели' value='' name='" + dataRow + "[" + cCount + "][targetID]' />\n\
                            <input class='descrTerget' type='text' placeholder='Название цели' value='' name='" + dataRow + "[" + cCount + "][descrTerget]' />\n\
                        </div>";

        $($lastRow).after(row);

        $('#yam .tergetRow').last().append('<span class="delete_row">Удалить<span>')
        addBtn();
    });

    $('#yam').on('click', '.delete_row', function () {
        if ($('#yam .tergetRow').length > 1) {
            $(this).parents('.tergetRow').remove();
            addBtn();
        } else {
            $('#yam .tergetRow').find('input').val('');
        }



    });

});